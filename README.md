# 蓝莓blueberryF411飞控简介

#### 介绍
{**常见问题&使用说明[https://gitee.com/sun_yan_meng/blueberryF411/wikis](https://gitee.com/sun_yan_meng/blueberryF411/wikis)**}

blueberryF411是 blueberry团队推出的基于各大开源项飞控项目的mini飞控平台,它的尺寸仅有20mmx35mmx9mm,孔间距16*16mm。专为FPV玩家提供稳定、高效、安全、易上手的固定翼FPV飞控平台
无论是个人、团队、或是企业，都能够用 blueberry飞控实现自动稳定、失控返航、航点飞行等功能。
![blueberryF411](%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_17053767099181.png)
![66](%E5%9B%BE%E7%89%87.png)

#### 支持固件
INAV{**[https://github.com/iNavFlight](https://github.com/iNavFlight)**} 

Betaflight{**[https://github.com/Betaflight](https://github.com/Betaflight)**}

#### 主流功能

集成模拟图传信息显示(OSD) 
支持DJI高清图传,蜗牛高清图传OSD 
OSD信息:所有飞控信息，如飞行高度,航向,电池电压电流,飞行总里程，返航距离,返航方向,和航路点任务...等
优秀的固定翼无人机支持:飞控混控可以随意定义,理论上支持任意机型的控制，如单发,双发差速控制,飞翼布局,鸭翼布局,甚至直升机,车船.
允许运行任何硬件，你想:多旋翼，固定翼，漫游者，船和其他实验设备
多种传感器支持:GPS，空速计，声纳，激光雷达，温度，ESC与BIHeli_32遥测SmartAudio和IRC Tramp VTX...等
支持黑匣子飞行记录器记录
多种数传协议：SmartPort、FPort、MAVlink、LTM、CRSF多色RGB LED
除了此基础功能外还支持各种功能,在此不一一列举了

####端口定义
![输入图片说明](%E5%89%AF%E6%9C%AC_%E5%89%AF%E6%9C%AC_%E5%89%AF%E6%9C%AC_%E6%8E%A5%E5%8F%A3%E5%AE%9A%E4%B9%89%E5%9B%BE%E7%99%BD%E8%89%B2%E8%83%8C%E6%99%AF%E5%9B%BE.png)
#### 安装教程

1.  如果使用INAV固件,固件版本与地面站版本需要保持一致,否则无法连接
2.  地面站安装

3.  INAV{**[https://github.com/iNavFlight/inav-configurator/releases](https://github.com/iNavFlight/inav-configurator/releases)**} 

4.  Betaflight{**[https://github.com/betaflight/betaflight-configurator/releases](https://github.com/betaflight/betaflight-configurator/releases)**}

5.  驱动安装教程{**[https://www.bilibili.com/read/cv17581851/](https://www.bilibili.com/read/cv17581851/)**}

#### 视频教程

1.  硬件安装教程
【手抛机改小小小胖，一节18650续航1小时，飞40km，教程之电路部分上-哔哩哔哩】 https://b23.tv/tW8kUy7
【手抛机改小小小胖一节18650续航1小时，飞40km，教程之电路篇（下）-哔哩哔哩】 https://b23.tv/BXHKb4N
2.  INAV基础教程
3.  INAV基础教程

通用教程
【inav固定翼小白教程1-哔哩哔哩】 https://b23.tv/8O6RMd1
【inav固定翼小白教程2-哔哩哔哩】 https://b23.tv/1mJgP87
【1节18650电池飞行1小时！！手抛机改小小小胖FPv载机，最长续航翼展48cm超级小-哔哩哔哩】 https://b23.tv/ddAM91p
【一节18650，爽飞一下午-哔哩哔哩】 https://b23.tv/behkaNc
【48手抛机改小小小胖，一节18650续航1小时，飞40km，全套教程之设备准备篇（一）-哔哩哔哩】 https://b23.tv/wUJ42tI
【为inav打call！固定翼inav地面站调参教程，新手inav入门教程】 https://www.bilibili.com/video/BV16L411d7JA/?share_source=copy_web&vd_source=310ee154a60985c6d981ec0fea0f81c6
开源飞控固件通用性很强，跟飞控硬件不绑定，inav教程都是通用的，更多资料可以自行查找。

#### 交流群

QQ1群:513532729

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
